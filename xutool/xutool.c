/*
 *  V4L2 video capture example
 *
 *  This program can be used and distributed without restrictions.
 *
 *      This program is provided with the V4L2 API
 * see http://linuxtv.org/docs.php for more information
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>

#include <getopt.h>             /* getopt_long() */

#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <linux/uvcvideo.h>
#include <linux/videodev2.h>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

#ifndef V4L2_PIX_FMT_H264
#define V4L2_PIX_FMT_H264     v4l2_fourcc('H', '2', '6', '4')   /* H264 with start codes */
#endif

#define MODULE_NAME			"xutool"
#define MODULE_VERSION		"L0.01"

enum io_method {
    IO_METHOD_READ,
    IO_METHOD_MMAP,
    IO_METHOD_USERPTR,
};

struct buffer {
    void *start;
    size_t length;
};

static char *dev_name;
static enum io_method io = IO_METHOD_MMAP;
static int fd = -1;
struct buffer *buffers;
static unsigned int n_buffers;
static int out_buf;
static int force_format;
static int frame_count = 200;
static int frame_number = 0;

static void errno_exit(const char *s)
{
    fprintf(stderr, "%s error %d, %s\n", s, errno, strerror(errno));
    exit(EXIT_FAILURE);
}

static int xioctl(int fh, int request, void *arg)
{
    int r;

    do {
        r = ioctl(fh, request, arg);
    } while (-1 == r && EINTR == errno);

    return r;
}

static void process_image(const void *p, int size)
{
    frame_number++;
    char filename[15];
    sprintf(filename, "frame-%d.raw", frame_number);
    FILE *fp = fopen(filename, "wb");

    if (out_buf)
        fwrite(p, size, 1, fp);

    fflush(fp);
    fclose(fp);
}

static int read_frame(void)
{
    struct v4l2_buffer buf;
    unsigned int i;

    switch (io) {
        case IO_METHOD_READ:
            if (-1 == read(fd, buffers[0].start, buffers[0].length)) {
                switch (errno) {
                    case EAGAIN:
                        return 0;

                    case EIO:
                        /* Could ignore EIO, see spec. */

                        /* fall through */

                    default:
                        errno_exit("read");
                }
            }

            process_image(buffers[0].start, buffers[0].length);
            break;

        case IO_METHOD_MMAP:
            CLEAR(buf);

            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;

            if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
                switch (errno) {
                    case EAGAIN:
                        return 0;

                    case EIO:
                        /* Could ignore EIO, see spec. */

                        /* fall through */

                    default:
                        errno_exit("VIDIOC_DQBUF");
                }
            }

            assert(buf.index < n_buffers);

            process_image(buffers[buf.index].start, buf.bytesused);

            if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
                errno_exit("VIDIOC_QBUF");
            break;

        case IO_METHOD_USERPTR:
            CLEAR(buf);

            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_USERPTR;

            if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
                switch (errno) {
                    case EAGAIN:
                        return 0;

                    case EIO:
                        /* Could ignore EIO, see spec. */

                        /* fall through */

                    default:
                        errno_exit("VIDIOC_DQBUF");
                }
            }

            for (i = 0; i < n_buffers; ++i)
                if (buf.m.userptr == (unsigned long) buffers[i].start
                    && buf.length == buffers[i].length)
                    break;

            assert(i < n_buffers);

            process_image((void *) buf.m.userptr, buf.bytesused);

            if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
                errno_exit("VIDIOC_QBUF");
            break;
    }

    return 1;
}

static void mainloop(void)
{
    unsigned int count;

    count = frame_count;

    while (count-- > 0) {
        for (;;) {
            fd_set fds;
            struct timeval tv;
            int r;

            FD_ZERO(&fds);
            FD_SET(fd, &fds);

            /* Timeout. */
            tv.tv_sec = 2;
            tv.tv_usec = 0;

            r = select(fd + 1, &fds, NULL, NULL, &tv);

            if (-1 == r) {
                if (EINTR == errno)
                    continue;
                errno_exit("select");
            }

            if (0 == r) {
                fprintf(stderr, "select timeout\n");
                exit(EXIT_FAILURE);
            }

            if (read_frame())
                break;
            /* EAGAIN - continue select loop. */
        }
    }
}

static void stop_capturing(void)
{
    enum v4l2_buf_type type;

    switch (io) {
        case IO_METHOD_READ:
            /* Nothing to do. */
            break;

        case IO_METHOD_MMAP:
        case IO_METHOD_USERPTR:
            type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            if (-1 == xioctl(fd, VIDIOC_STREAMOFF, &type))
                errno_exit("VIDIOC_STREAMOFF");
            break;
    }
}

static void start_capturing(void)
{
    unsigned int i;
    enum v4l2_buf_type type;

    switch (io) {
        case IO_METHOD_READ:
            /* Nothing to do. */
            break;

        case IO_METHOD_MMAP:
            for (i = 0; i < n_buffers; ++i) {
                struct v4l2_buffer buf;

                CLEAR(buf);
                buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory = V4L2_MEMORY_MMAP;
                buf.index = i;

                if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
                    errno_exit("VIDIOC_QBUF");
            }
            type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            if (-1 == xioctl(fd, VIDIOC_STREAMON, &type))
                errno_exit("VIDIOC_STREAMON");
            break;

        case IO_METHOD_USERPTR:
            for (i = 0; i < n_buffers; ++i) {
                struct v4l2_buffer buf;

                CLEAR(buf);
                buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory = V4L2_MEMORY_USERPTR;
                buf.index = i;
                buf.m.userptr = (unsigned long) buffers[i].start;
                buf.length = buffers[i].length;

                if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
                    errno_exit("VIDIOC_QBUF");
            }
            type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            if (-1 == xioctl(fd, VIDIOC_STREAMON, &type))
                errno_exit("VIDIOC_STREAMON");
            break;
    }
}

static void uninit_device(void)
{
    unsigned int i;

    switch (io) {
        case IO_METHOD_READ:
            free(buffers[0].start);
            break;

        case IO_METHOD_MMAP:
            for (i = 0; i < n_buffers; ++i)
                if (-1 == munmap(buffers[i].start, buffers[i].length))
                    errno_exit("munmap");
            break;

        case IO_METHOD_USERPTR:
            for (i = 0; i < n_buffers; ++i)
                free(buffers[i].start);
            break;
    }

    free(buffers);
}

static void init_read(unsigned int buffer_size)
{
    buffers = calloc(1, sizeof(*buffers));

    if (!buffers) {
        fprintf(stderr, "Out of memory\n");
        exit(EXIT_FAILURE);
    }

    buffers[0].length = buffer_size;
    buffers[0].start = malloc(buffer_size);

    if (!buffers[0].start) {
        fprintf(stderr, "Out of memory\n");
        exit(EXIT_FAILURE);
    }
}

static void init_mmap(void)
{
    struct v4l2_requestbuffers req;

    CLEAR(req);

    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
        if (EINVAL == errno) {
            fprintf(stderr, "%s does not support "
                    "memory mapping\n", dev_name);
            exit(EXIT_FAILURE);
        } else {
            errno_exit("VIDIOC_REQBUFS");
        }
    }

    if (req.count < 2) {
        fprintf(stderr, "Insufficient buffer memory on %s\n", dev_name);
        exit(EXIT_FAILURE);
    }

    buffers = calloc(req.count, sizeof(*buffers));

    if (!buffers) {
        fprintf(stderr, "Out of memory\n");
        exit(EXIT_FAILURE);
    }

    for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {
        struct v4l2_buffer buf;

        CLEAR(buf);

        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = n_buffers;

        if (-1 == xioctl(fd, VIDIOC_QUERYBUF, &buf))
            errno_exit("VIDIOC_QUERYBUF");

        buffers[n_buffers].length = buf.length;
        buffers[n_buffers].start = mmap(NULL /* start anywhere */ ,
                                        buf.length,
                                        PROT_READ | PROT_WRITE
                                        /* required */ ,
                                        MAP_SHARED /* recommended */ ,
                                        fd, buf.m.offset);

        if (MAP_FAILED == buffers[n_buffers].start)
            errno_exit("mmap");
    }
}

static void init_userp(unsigned int buffer_size)
{
    struct v4l2_requestbuffers req;

    CLEAR(req);

    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_USERPTR;

    if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
        if (EINVAL == errno) {
            fprintf(stderr, "%s does not support "
                    "user pointer i/o\n", dev_name);
            exit(EXIT_FAILURE);
        } else {
            errno_exit("VIDIOC_REQBUFS");
        }
    }

    buffers = calloc(4, sizeof(*buffers));

    if (!buffers) {
        fprintf(stderr, "Out of memory\n");
        exit(EXIT_FAILURE);
    }

    for (n_buffers = 0; n_buffers < 4; ++n_buffers) {
        buffers[n_buffers].length = buffer_size;
        buffers[n_buffers].start = malloc(buffer_size);

        if (!buffers[n_buffers].start) {
            fprintf(stderr, "Out of memory\n");
            exit(EXIT_FAILURE);
        }
    }
}

static void init_device(void)
{
    struct v4l2_capability cap;
    struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;
    struct v4l2_format fmt;
    unsigned int min;

    if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &cap)) {
        if (EINVAL == errno) {
            fprintf(stderr, "%s is no V4L2 device\n", dev_name);
            exit(EXIT_FAILURE);
        } else {
            errno_exit("VIDIOC_QUERYCAP");
        }
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        fprintf(stderr, "%s is no video capture device\n", dev_name);
        exit(EXIT_FAILURE);
    }

    switch (io) {
        case IO_METHOD_READ:
            if (!(cap.capabilities & V4L2_CAP_READWRITE)) {
                fprintf(stderr, "%s does not support read i/o\n",
                        dev_name);
                exit(EXIT_FAILURE);
            }
            break;

        case IO_METHOD_MMAP:
        case IO_METHOD_USERPTR:
            if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
                fprintf(stderr, "%s does not support streaming i/o\n",
                        dev_name);
                exit(EXIT_FAILURE);
            }
            break;
    }


    /* Select video input, video standard and tune here. */


    CLEAR(cropcap);

    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (0 == xioctl(fd, VIDIOC_CROPCAP, &cropcap)) {
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect;       /* reset to default */

        if (-1 == xioctl(fd, VIDIOC_S_CROP, &crop)) {
            switch (errno) {
                case EINVAL:
                    /* Cropping not supported. */
                    break;
                default:
                    /* Errors ignored. */
                    break;
            }
        }
    } else {
        /* Errors ignored. */
    }


    CLEAR(fmt);

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (force_format) {
        fprintf(stderr, "Set H264\r\n");
        fmt.fmt.pix.width = 640;        //replace
        fmt.fmt.pix.height = 480;       //replace
        fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_H264;    //replace
        fmt.fmt.pix.field = V4L2_FIELD_ANY;

        if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt))
            errno_exit("VIDIOC_S_FMT");

        /* Note VIDIOC_S_FMT may change width and height. */
    } else {
        /* Preserve original settings as set by v4l2-ctl for example */
        if (-1 == xioctl(fd, VIDIOC_G_FMT, &fmt))
            errno_exit("VIDIOC_G_FMT");
    }

    /* Buggy driver paranoia. */
    min = fmt.fmt.pix.width * 2;
    if (fmt.fmt.pix.bytesperline < min)
        fmt.fmt.pix.bytesperline = min;
    min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
    if (fmt.fmt.pix.sizeimage < min)
        fmt.fmt.pix.sizeimage = min;

    switch (io) {
        case IO_METHOD_READ:
            init_read(fmt.fmt.pix.sizeimage);
            break;

        case IO_METHOD_MMAP:
            init_mmap();
            break;

        case IO_METHOD_USERPTR:
            init_userp(fmt.fmt.pix.sizeimage);
            break;
    }
}

static void close_device(void)
{
    if (-1 == fd) {
        return;
    }

    if (-1 == close(fd))
        errno_exit("close");

    fd = -1;
}

static void open_device(void)
{
    struct stat st;

    if (-1 == stat(dev_name, &st)) {
        fprintf(stderr, "Cannot identify '%s': %d, %s\n",
                dev_name, errno, strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (!S_ISCHR(st.st_mode)) {
        fprintf(stderr, "%s is no device\n", dev_name);
        exit(EXIT_FAILURE);
    }

    fd = open(dev_name, O_RDWR /* required */  | O_NONBLOCK, 0);

    if (-1 == fd) {
        fprintf(stderr, "Cannot open '%s': %d, %s\n",
                dev_name, errno, strerror(errno));
        exit(EXIT_FAILURE);
    }

    //fprintf(stderr, "%s(): fd = %d\n", __FUNCTION__, fd);
}

static void usage()
{
    printf("\n");
    printf("  [%s version %s]\n", MODULE_NAME, MODULE_VERSION);
    printf("  -------------------------------------------------\n");
    printf("  %s <command> <-r/-d> <arg1> <arg2> ...\n", MODULE_NAME);
    printf("\n");
    printf("  command,\n");
    printf("    -d <path of device node, like /dev/video0>\n");
    printf("    -p <apollo|hubble>\n");
    printf("    eng_get <id_in_decimal> <size_in_decimal_to_get>\n");
    printf("    eng_set <id_in_decimal> <data1_in_hex> <data2_in_hex> <data3_in_hex> ....\n");
    printf("\n");
}

enum cmd_type_e {
	CMD_NONE = 0,
	CMD_ENG_GET,
	CMD_ENG_SET,
};

#define HAS_ARG_NUM(argc, argnow, need) ((argnow + need) < argc)

int main(int argc, char **argv)
{
    unsigned int unCmd = CMD_NONE;
    unsigned int unArgNow = 0;

    unsigned char buff[512];
    unsigned int buffLen = 0;

    unsigned int unXuId;

    struct uvc_xu_control_query xu;
    int selector = 0;

    xu.unit = 0x0A;

    dev_name = "/dev/video0";

    while (unArgNow < argc) {
        if (strcmp(argv[unArgNow], "-d") == 0) {
            if (!HAS_ARG_NUM(argc, unArgNow, 1)) break;
            unArgNow++;
            dev_name = argv[unArgNow++];
		}
        else if (strcmp(argv[unArgNow], "-p") == 0) {
            if (!HAS_ARG_NUM(argc, unArgNow, 1)) break;
            unArgNow++;
            if (strcmp(argv[unArgNow++], "apollo") == 0) {
                xu.unit = 0x03;
            }
            else {
                xu.unit = 0x0A;
            }
		}
		else if (strcmp(argv[unArgNow], "eng_get") == 0) {
			if (!HAS_ARG_NUM(argc, unArgNow, 2)) break;
			unArgNow++;
			unXuId = atoi(argv[unArgNow++]);
			buffLen = atoi(argv[unArgNow++]);
            unCmd = CMD_ENG_GET;
		}
		else if (strcmp(argv[unArgNow], "eng_set") == 0) {
			if (!HAS_ARG_NUM(argc, unArgNow, 2)) break; /* at least, two bytes data are needed */
			unArgNow++;
			unXuId = atoi(argv[unArgNow++]);
			buffLen = argc - unArgNow;
			for (int i=0; i<buffLen; i++) {
                buff[i] = strtol(argv[unArgNow++], NULL, 16);
                //printf("%02d: 0x%02x\n", i, buff[i]);
                //printf("%02x ", buff[i]);
			}
            //printf("\n");
            unCmd = CMD_ENG_SET;
		}
        else { 
            unArgNow++;
        }
    }

    //printf("unCmd = %d\n", unCmd);
    //printf("xu.unit = %d\n", xu.unit);

    printf("Device: %s\n", dev_name);

    if (CMD_ENG_GET == unCmd) {
        int err = 0;
        uint16_t len = 0;

        open_device();

        xu.query = 0x85; /* UVC_GET_LEN */
        xu.selector = unXuId;
        xu.size = sizeof(len);
        xu.data = (unsigned char *) &len;
        err = xioctl(fd, UVCIOC_CTRL_QUERY, &xu);
        if (err) {
            printf("UVC_GET_LEN error(%d), exit!!!\n", err);
            close_device();
            exit(EXIT_FAILURE);
        }

        if (buffLen != (uint32_t)len) {
            printf("Request length (%d) is differnt with the value from UVC_GET_LEN (%d), exit!!!\n", buffLen, len);
            close_device();
            exit(EXIT_FAILURE);
        }

        memset(buff, 0, sizeof(buff));
        xu.query = 0x81; /* UVC_GET_CUR */
        xu.size = len;
        xu.data = buff;
        err = xioctl(fd, UVCIOC_CTRL_QUERY, &xu);
        if (err) {
            printf("UVC_GET_CUR error(%d), exit!!!\n", err);
            close_device();
            exit(EXIT_FAILURE);
        }

        for (int i=0; i<len; i++) {
            printf("%02x ", buff[i]);
        }

        printf("\n");

        close_device();
    }
    else if (CMD_ENG_SET == unCmd) {
        printf("ENG_SET\n");

        int err = 0;
        uint16_t len = 128;

        open_device();

        // xu.query = 0x85; /* UVC_GET_LEN */
        xu.selector = unXuId;
        // xu.size = sizeof(len);
        // xu.data = (unsigned char *) &len;
        // err = xioctl(fd, UVCIOC_CTRL_QUERY, &xu);
        // if (err) {
        //     printf("UVC_GET_LEN error(%d), exit!!!\n", err);
        //     close_device();
        //     exit(EXIT_FAILURE);
        // }

        // if (buffLen != (uint32_t)len) {
        //     printf("Request length (%d) is differnt with the value from UVC_GET_LEN (%d), exit!!!\n", buffLen, len);
        //     close_device();
        //     exit(EXIT_FAILURE);
        // }

        xu.query = 0x01; /* UVC_SET_CUR */
        xu.size = len;
        xu.data = buff;
        err = xioctl(fd, UVCIOC_CTRL_QUERY, &xu);
        if (err) {
            printf("UVC_SET_CUR error(%d), exit!!!\n", err);
            close_device();
            exit(EXIT_FAILURE);
        }

        close_device();
    }
    else {
        usage();
    }

    return 0;
}

