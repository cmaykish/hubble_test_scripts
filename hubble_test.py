import time
import subprocess
import proto.host2cam_pb2 as host2cam

test_delay_s = 3

# xutool_cmd = "/home/colin/Desktop/hubble_test_scripts/xutool -p hubble "
xutool_cmd = "binaries/xutool_win.exe -p hubble "
xu_set = "eng_set "
xu_get = "eng_get "
xu23 = "23 "
xu24 = "24 "
sync_bytes = "BE EF "
xu_buffer_size = 128

def run_xutool(params):
    print("Calling: " + xutool_cmd + params)
    cmd = (xutool_cmd + params).split()
    proc = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    stdout, stderr = proc.communicate()
    result = stdout.decode()

    print(result)

    return result

def xu23_set_cur(binary):
    run_xutool(xu_set + xu23 + binary)

def xu24_get_cur():
    return run_xutool(xu_get + xu24)

# Convert a binary string to an ASCII string in HEX format, e.g. A0 B0 C0 FF 00
def bin_to_string(binary_string):
    output = ""
    for c in binary_string:
        output += format(c, "X") + " "
    return output

def start_recording():
    print("Start recording")

    # Create a start-recording protobuf command
    start_command = host2cam.Host2Cam()
    start_command.time.GetCurrentTime()
    start_command.sequence_number = 1
    start_command.start_recording.CopyFrom(host2cam.StartRecording())

    # Serialize protobuf command to binary
    start_bin = start_command.SerializeToString()

    start_bin_string = sync_bytes + "12 " + bin_to_string(start_bin)

    print(start_bin_string)

    # ugly
    while start_bin_string.count(' ') < xu_buffer_size:
        start_bin_string += " 00"

    print(start_bin_string)

    xu23_set_cur(start_bin_string)

def stop_recording():
    print("Stop recording")

    # Create a stop-recording protobuf command
    stop_command = host2cam.Host2Cam()
    stop_command.time.GetCurrentTime()
    stop_command.sequence_number = 1
    stop_command.stop_recording.CopyFrom(host2cam.StopRecording())

    # Serialize protobuf command to binary
    stop_bin = stop_command.SerializeToString()

    stop_bin_string = sync_bytes + "12 "

    for c in stop_bin:
        stop_bin_string += format(c, "X") + " "

    print(stop_bin_string)

    # ugly
    while stop_bin_string.count(' ') < xu_buffer_size:
        stop_bin_string += " 00"

    xu23_set_cur(stop_bin_string)

def main():
    print("Hubble Production Test Script")

    # Tests
    stop_recording()
    time.sleep(test_delay_s)

    start_recording()
    time.sleep(test_delay_s)

if __name__== "__main__":
  main()